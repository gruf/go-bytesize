package bytesize_test

import (
	"errors"
	"testing"

	"codeberg.org/gruf/go-bytesize"
)

const (
	KB = 1e3
	MB = 1e6
	GB = 1e9
	TB = 1e12
	PB = 1e15
	EB = 1e18

	KiB = 1024
	MiB = KiB * 1024
	GiB = MiB * 1024
	TiB = GiB * 1024
	PiB = TiB * 1024
	EiB = PiB * 1024
)

var formatTests = []struct {
	Value     bytesize.Size
	StringSI  string
	StringIEC string
}{
	{
		Value:     1,
		StringSI:  `1B`,
		StringIEC: `1B`,
	},
	{
		Value:     1 * KiB,
		StringSI:  `1.02kB`,
		StringIEC: `1.00kiB`,
	},
	{
		Value:     1 * MiB,
		StringSI:  `1.05MB`,
		StringIEC: `1.00MiB`,
	},
	{
		Value:     1 * GiB,
		StringSI:  `1.07GB`,
		StringIEC: `1.00GiB`,
	},
	{
		Value:     1999,
		StringSI:  `2.00kB`,
		StringIEC: `1.95kiB`,
	},
}

var parseTests = []struct {
	String string
	Value  bytesize.Size
	Error  error
}{
	{
		String: `0`,
		Value:  0,
	},
	{
		String: `1024`,
		Value:  1024,
	},
	{
		String: `1024B`,
		Value:  1024,
	},
	{
		String: `1kiB`,
		Value:  1 * KiB,
	},
	{
		String: `1kB`,
		Value:  1 * KB,
	},
	{
		String: `1.22kB`,
		Value:  1.22 * KB,
	},
	{
		String: `1.22kiB`,
		Value:  floatSz(1.22 * float64(bytesize.KiB)),
	},
	{
		String: `1.69GiB`,
		Value:  floatSz(1.69 * float64(bytesize.GiB)),
	},
	{
		String: `0.89TiB`,
		Value:  floatSz(0.89 * float64(bytesize.TiB)),
	},
	{
		String: `93.541PiB`,
		Value:  floatSz(93.541 * float64(bytesize.PiB)),
	},
	{
		String: `93abPiB`,
		Error:  bytesize.ErrInvalidFormat,
	},
	{
		String: `93abZiB`,
		Error:  bytesize.ErrInvalidUnit,
	},
}

func TestFormat(t *testing.T) {
	for _, test := range formatTests {
		if s := test.Value.StringSI(); s != test.StringSI {
			t.Fatalf("expected SI strings do not match: expect=%q receive=%q", test.StringSI, s)
		} else if s := test.Value.StringIEC(); s != test.StringIEC {
			t.Fatalf("expected IEC strings do not match: expect=%q receive=%q", test.StringIEC, s)
		}
	}
}

func TestParse(t *testing.T) {
	for _, test := range parseTests {
		v, err := bytesize.ParseSize(test.String)
		if (v != test.Value) || !errors.Is(err, test.Error) {
			t.Fatalf("parsed size / error does not match expected: expect=(%v, %q) receive=(%v, %q)", test.Value, test.Error, v, err)
		}
	}
}

func BenchmarkFormatIEC(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var s1, s2 string

		for pb.Next() {
			for _, test := range formatTests {
				s1 = test.Value.StringIEC()
			}
		}

		s1, s2 = s2, s1
		_ = s1
		_ = s2
	})
}

func BenchmarkFormatSI(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var s1, s2 string

		for pb.Next() {
			for _, test := range formatTests {
				s2 = test.Value.StringSI()
			}
		}

		s1, s2 = s2, s1
		_ = s1
		_ = s2
	})
}

func BenchmarkParseIEC(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var sz1, sz2 bytesize.Size

		for pb.Next() {
			for _, test := range formatTests {
				sz1, _ = bytesize.ParseSize(test.StringIEC)
			}
		}

		sz1, sz2 = sz2, sz1
		_ = sz1
		_ = sz2
	})
}

func BenchmarkParseSI(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		var sz1, sz2 bytesize.Size

		for pb.Next() {
			for _, test := range formatTests {
				sz2, _ = bytesize.ParseSize(test.StringSI)
			}
		}

		sz1, sz2 = sz2, sz1
		_ = sz1
		_ = sz2
	})
}

func floatSz(f float64) bytesize.Size {
	return bytesize.Size(f)
}
